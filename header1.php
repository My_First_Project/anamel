<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <meta name="viewport" content="width=device-width"/>
<!--    <style>-->
<!--        .popup{-->
<!--            position:relative;-->
<!--            display:inline-block;-->
<!--            cursor:pointer;-->
<!--            -webkit-user-select:none;-->
<!--            -moz-user-select:none;-->
<!--            -ms-user-select:none;-->
<!--            user-select:none;-->
<!--        }-->
<!--        .popup .popuptext{-->
<!--            visiibility:hidden;-->
<!--            width:160px;-->
<!--            background-color:#555;-->
<!--            color:#fff;-->
<!--            text-align:center;-->
<!--            border-radius:6px;-->
<!--            padding:8px 0;-->
<!--            position:absolute;-->
<!--            z-index:1;-->
<!--            bottom:125%;-->
<!--            left:50%;-->
<!--            margin-left:-80px;-->
<!--        }-->
<!--        .popup .popuptext::after{-->
<!--            content:"";-->
<!--            position:absolute;-->
<!--            left:50%;-->
<!--            top:100%;-->
<!--            margin-left;-5px;-->
<!--            border-width:5px;-->
<!--            border-style:solid;-->
<!--            border-color:#555 transparent transparent transparent;-->
<!---->
<!--        }-->
<!--        .popup .show{-->
<!--            visibility:visible;-->
<!--            -webkit-animation:fadeIn 1s;-->
<!--            animation:fadeIn 1s;-->
<!--        }-->
<!--        @-webkit-keyframes fadeIn{-->
<!--            from{opacity:0;}-->
<!--            to{opacity:1;}-->
<!--        }-->
<!--        @keyframes fadeIn{-->
<!--            from{opacity:0;}-->
<!--            to{opacity:1;}-->
<!--        }-->
<!---->
<!--    </style>-->
<!--</head>-->
<!--<body style="text-align:center">-->
<!--    <h2>Pop Up</h2>-->
<!--    <div class="popup" onclick="myFunction()">Click me to toggle the pop up!-->
<!--        <span class="popuptext" id="myPopup">A simple pop up</span>-->
<!--    </div>-->
<!--    <script>-->
<!--        function myfunction{-->
<!--            var popup=document.getElementById("mypopup");-->
<!--            popup.classlist.toggle("show");-->
<!--        }-->
<!--    </script>-->

        <style>
            /* Popup container - can be anything you want */
            .popup {
                position: relative;
                display: inline-block;
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            /* The actual popup */
            .popup .popuptext {
                visibility: hidden;
                width: 360px;
                height:auto;
                background-color: #555;
                color: #fff;
                text-align: center;
                border-radius: 6px;
                padding: 8px 0;
                position: absolute;
                z-index: 1;
                top: 125%;
                left: 50%;
                margin-left: -80px;

            }

            /* Popup arrow */
            .popup .popuptext::after {
                content: "";
                position: absolute;
                top: 100%;
                left: 50%;
                margin-left: -5px;
                border-width: 5px;
                border-style: solid;
                border-color: #555 transparent transparent transparent;
            }

            /* Toggle this class - hide and show the popup */
            .popup .show {
                visibility: visible;
                -webkit-animation: fadeIn 1s;
                animation: fadeIn 1s;
            }

            /* Add animation (fade in the popup) */
            @-webkit-keyframes fadeIn {
                from {opacity: 0;}
                to {opacity: 1;}
            }

            @keyframes fadeIn {
                from {opacity: 0;}
                to {opacity:1 ;}
            }
        </style>
    </head>
<body style="text-align:center">

<h2>Popup</h2>

<div class="popup" onclick="myFunction()">
    <button class="btn btn-primary btn-sm pull-right" data-target="#loginmodel" data-toggle="modal">SIGN IN</button>
    <span class="popuptext" id="myPopup"><input type="text" class="form-control" placeholder="Login Username" id="inputUserName"/>
        <input type="password" class="form-control" placeholder="Login Password" id="inputPassword"/>
    </span>
</div>

<script>
    // When the user clicks on div, open the popup
    function myFunction() {
        var popup = document.getElementById("myPopup");
        popup.classList.toggle("show");
    }
</script>

</body>
</html>


</body>
</html>