<html>
<head>
    <title>bootable</title>
    <link rel="shortcut icon" href="">
    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <script src="js/bootstrap.js"></script>
    <meta name="viewport" content="width=device-width"/>
</head>
<body>
    <table class="table table-hover table-bordered table-striped">
        <thead>
        <tr>
            <th>NAME</th>
            <th>CLASS</th>
            <th>COLLEGE</th>
            <th>AGE</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Britney</td>
            <td>7th sem</td>
            <td>Oxford</td>
            <td>17</td>
        </tr>
        <tr>
            <td>Sid</td>
            <td>6th sem</td>
            <td>Harvard</td>
            <td>16</td>
        </tr>
        <tr>
            <td>Mark</td>
            <td>8th sem</td>
            <td>Mgmt of dubai</td>
            <td>20</td>
        </tr>
        <tr>
            <td>Louis</td>
            <td>7th sem</td>
            <td>Oxford</td>
            <td>21</td>
        </tr>
        <tr>
            <td>Ray</td>
            <td>6th sem</td>
            <td>Harvard</td>
            <td>19</td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <th>NAME</th>
            <th>CLASS</th>
            <th>COLLEGE</th>
            <th>AGE</th>
        </tr>
        </tfoot>
    </table>
    </div>
</body>
</html>