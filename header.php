<html>
<head>
    <title>header</title>
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <meta name="viewport" content="width=device-width"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <script>
        $(function(){
            $("#visible").click(function(){
                $("#invisble").toggleClass("show");
            });
        });
    </script>
    <style>
        body{
            background-image:url('images/header1.jpg');
            background-size:cover;
            color:white;
        }
        ul{
            margin-top:-5px;
            padding:0px;
            list-style:none;
            line-height:60px;
        }
        ul li{
            background:black;
            opacity:0.8;
            width:130px;
            float:left;
            text-align:center;
            margin-right:2px;
        }

        ul li a{
            text-decoration:none;
            color:white;
            display:block;
        }
        ul li a:hover
        {
            background-color:#615964;
        }
        ul li ul li{
            display:none;
        }
        ul li:hover ul li {
            display: block;
        }
        .bton{
            margin-left:600px;
            margin-top:1px;
        }
        .a{
            width:50px;
            background:darkgray;
            color:white;
            border-radius:3px;
        }


/*             /* Popup container - can be anything you want */*/
/*         .popup {*/
/*             position: relative;*/
/*             display: inline-block;*/
/*             cursor: pointer;*/
/*             -webkit-user-select: none;*/
/*             -moz-user-select: none;*/
/*             -ms-user-select: none;*/
/*             user-select: none;*/
/*         }*/
/**/
/*        /* The actual popup */*/
/*        .popup .popuptext {*/
/*            visibility: hidden;*/
/*            width: 360px;*/
/*            height:auto;*/
/*            background-color: #555;*/
/*            color: #fff;*/
/*            text-align: center;*/
/*            border-radius: 6px;*/
/*            padding: 8px 0;*/
/*            position: absolute;*/
/*            z-index: 1;*/
/*            top: 125%;*/
/*            left: 50%;*/
/*            margin-left: -80px;*/
/**/
/*        }*/
/**/
/*        /* Popup arrow */*/
/*        .popup .popuptext::after {*/
/*            content: "";*/
/*            position: absolute;*/
/*            top: 100%;*/
/*            left: 50%;*/
/*            margin-left: -5px;*/
/*            border-width: 5px;*/
/*            border-style: solid;*/
/*            border-color: #555 transparent transparent transparent;*/
/*        }*/
/**/
/*        /* Toggle this class - hide and show the popup */*/
/*        .popup .show {*/
/*            visibility: visible;*/
/*            -webkit-animation: fadeIn 1s;*/
/*            animation: fadeIn 1s;*/
/*        }*/
/**/
/*        /* Add animation (fade in the popup) */
/*        @-webkit-keyframes fadeIn {*/
/*            from {opacity: 0;}*/
/*            to {opacity: 1;}*/
/*        }*/
/**/
/*        @keyframes fadeIn {*/
/*            from {opacity: 0;}*/
/*            to {opacity:1 ;}*/
/*        }*/
   </style>
</head>
<body style="text-align:center">
    <nav>
        <ul class="ul">
            <li class="list">HOME
                <ul class="ul1">
                    <li class="li">Option 1</li>
                    <li class="li">Option 2</li>
                </ul>
            </li>
            <li class="list">PROFILE
                <ul class="ul1">
                    <li class="li">Option 1</li>
                    <li class="li">Option 2</li>
                    <li class="li">Option 3</li>
                </ul>
            </li>
            <li class="list">MENU
                <ul class="ul1">
                    <li class="li">Menu 1</li>
                    <li class="li">Menu 2</li>
                </ul>
            </li>
            <li class="list" style="height:55px;">ABOUT US</li>
        </ul>
    </nav>
    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-light btn-md bton" data-toggle="modal" data-target="#myModal">SIGN IN</button>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"style="color:black; margin-left:10px;">SIGN UP</h4>
                    <button type="button" class="close pull-right" data-dismiss="modal">x</button>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" placeholder="Login Username" id="inputUserName"/><br/>
                    <input type="password" class="form-control" placeholder="Login Password" id="inputPassword"/><br/>
                    <a class="a" href="register.php">Submit<a/>
                    <span><a href="#">Forgot Password?</a></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

<!--    <h2>Popup</h2>-->
<!---->
<!--    <div class="popup" onclick="myFunction()">-->
<!--        <button class="btn btn-primary btn-sm pull-right" data-target="#loginmodel" data-toggle="modal">SIGN IN</button>-->
<!--        <span class="popuptext" id="myPopup"><input type="text" class="form-control" placeholder="Login Username" id="inputUserName"/>-->
<!--        <input type="password" class="form-control" placeholder="Login Password" id="inputPassword"/>-->
<!--    </span>-->
<!--    </div>-->
<!---->
<!--    <script>-->
<!--        // When the user clicks on div, open the popup-->
<!--        function myFunction() {-->
<!--            var popup = document.getElementById("myPopup");-->
<!--            popup.classList.toggle("show");-->
<!--        }-->
<!--    </script>-->
</body>
</html>