<?php
//  include('header.php')
//?>
<html>
<head>
    <title>slider</title>
    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <meta name="viewport" content="width=device-width"/>
    <style>
        .main{
            background: black;
        }
        .carousel-item active{
            height:100%;
            width:100%;

        }
        .carousel-item {
            margin-left:200px;
            margin-top:100px;
            height:70%;
            width:70%;

        }

    </style>
</head>
<body>
<!--<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="images/wall.jpg" alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/wall1.jpg" alt="Second slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/wall2.jpg" alt="Third slide">
        </div>
    </div>
</div>-->
    <div id="main" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="main" data-slide-to="0" class="acive"></li>
            <li data-target="main" data-slide-to="1"></li>
            <li data-target="main" data-slide-to="2"></li>
        </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="images/wall.jpg"  alt="First Slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/wall1.jpg"  alt="Second Slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/wall2.jpg"  alt="Third Slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#main" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" area-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#main" role="button" data-slide="next">
                <span class="carousel-control-next-icon" area-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
    </div>
</body>
</html>
