<html>
<head>
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>

    <script type="text/javascript">
        function printstars(){
            var rows = document.getElementById("rows").value;
            // var cols = document.getElementById("cols").value;
            var results = document.getElementById("results");

            console.log('rows '+rows);
            var i, j;
            var value = '';
            if(rows==''){
                document.getElementById("error").innerHTML="invalid input";
                results.innerHTML = '';
                return;
            }
            document.getElementById("error").innerHTML="";
            for (i = 1; i <= rows; i++)
            {
                for (j = 1; j <= i; j++)
                {
                     value = value+' * ';
                    // document.write('*');
                }
                // document.write('<br/>');
                value = value+'<br/>';

            }
            results.innerHTML = value;

            // to display error

        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-12" style="margin-top: 2%">
            <div class="col-md-3 col-lg-3">
                <label>Rows</label>
                <input type="number" class="form-control" id="rows"/>

                <span id="error"></span>
            </div>

            <div class="col-md-3 col-lg-3" style="margin-top: 2%">
                <button class="btn btn-primary btn-md" onclick="printstars();">Print</button>
            </div>
        </div>
        <div class="col-md-12" style="margin-top: 2%">
            <div class="col-md-3">
            <p id="results">Results</p>
            </div>
        </div>
    </div>
</div>
<!--<div class="col-md-3 col-lg-3">
               <label>Cols</label>
               <input type="number" class="form-control" id="cols"/>
           </div>-->

</body>
</html>